using KS.Examples.DependencyInjection.Data;
using KS.Examples.DependencyInjection.Models;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace KS.Examples.DependencyInjection.Business.Tests
{
    public class ArticlesServiceTests
    {
        [Fact]
        public async Task AddInvalidTitleThrowsException()
        {
            // Arrange
            var articlesRepositoryMock = new Mock<IArticlesRepository>();
            var title = "some title";
            var article = new Article()
            {
                Title = title,
            };
            var titleCheckerMock = new Mock<ITitleChecker>();
            titleCheckerMock.Setup(t => t.IsAcceptable(It.IsAny<string>())).Returns(false);
            var articlesService = new ArticlesService(articlesRepositoryMock.Object, titleCheckerMock.Object);

            // Act
            Func<Task> action = () => articlesService.Add(article);

            // Assert
            articlesRepositoryMock.Verify(a => a.Add(article), Times.Never);
            await Assert.ThrowsAsync<ArgumentException>(action).ConfigureAwait(false);
        }

        [Fact]
        public async Task AddValidTitleReturnsArticle()
        {
            // Arrange
            var articlesRepositoryMock = new Mock<IArticlesRepository>();
            var title = "some title";
            var article = new Article()
            {
                Title = title,
            };
            articlesRepositoryMock.Setup(a => a.Add(article)).ReturnsAsync(article);
            var titleCheckerMock = new Mock<ITitleChecker>();
            titleCheckerMock.Setup(t => t.IsAcceptable(It.IsAny<string>())).Returns(true);
            var articlesService = new ArticlesService(articlesRepositoryMock.Object, titleCheckerMock.Object);

            // Act
            var result = await articlesService.Add(article).ConfigureAwait(false);

            // Assert
            articlesRepositoryMock.Verify(a => a.Add(article), Times.Once);
            Assert.Equal(title, result.Title);
        }

        [Fact]
        public async Task GetByIdExistingIdentifierReturnsArticle()
        {
            // Arrange
            var articlesRepositoryMock = new Mock<IArticlesRepository>();
            var existingIdentifier = Guid.NewGuid();
            var article = new Article()
            {
                Id = existingIdentifier,
            };
            articlesRepositoryMock.Setup(a => a.GetById(existingIdentifier)).ReturnsAsync(article);
            var titleCheckerMock = new Mock<ITitleChecker>();
            var articlesService = new ArticlesService(articlesRepositoryMock.Object, titleCheckerMock.Object);

            // Act
            var result = await articlesService.GetById(existingIdentifier).ConfigureAwait(false);

            // Assert
            Assert.True(existingIdentifier.Equals(result.Id));
        }
    }
}