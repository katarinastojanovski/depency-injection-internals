using KS.Examples.DependencyInjection.Business;
using KS.Examples.DependencyInjection.Controllers;
using KS.Examples.DependencyInjection.Data;
using KS.Examples.DependencyInjection.DI;
using KS.Examples.DependencyInjection.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace KS.Examples.DependencyInjection.IntegrationTests
{
    public class ArticlesControllerIntegrationTests
    {
        [Fact]
        public async Task GetExistingIdentifierReturnsArticle()
        {
            // Arrange
            var existingIdentifier = Guid.NewGuid();
            var articlesRepositoryMock = new Mock<IArticlesRepository>();
            var article = new Article
            {
                Id = existingIdentifier,
            };
            articlesRepositoryMock.Setup(a => a.GetById(existingIdentifier)).ReturnsAsync(article);

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddMyServices();

            // It is also possible to remove the existing implemention of IArticlesRepository from the service collection.
            // It will work just fine without removing it first.
            serviceCollection.AddSingleton(articlesRepositoryMock.Object);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var articlesService = serviceProvider.GetService<IArticlesService>();

            var articlesController = new ArticlesController(articlesService);

            // Act
            var result = await articlesController.Get(existingIdentifier).ConfigureAwait(false);

            // Assert
            // We need to do result.Result since we are using IActionResult<T> and not just IActionResult.
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var returnedArticle = Assert.IsType<Article>(okResult.Value);
            Assert.True(existingIdentifier.Equals(returnedArticle.Id));
        }
    }
}