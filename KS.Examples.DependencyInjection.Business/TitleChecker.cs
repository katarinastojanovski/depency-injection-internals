﻿namespace KS.Examples.DependencyInjection.Business
{
    internal class TitleChecker : ITitleChecker
    {
        public bool IsAcceptable(string title)
        {
            return title.ToUpperInvariant() != "test";
        }
    }
}