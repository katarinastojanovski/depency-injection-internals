﻿namespace KS.Examples.DependencyInjection.Business
{
    internal interface ITitleChecker
    {
        bool IsAcceptable(string title);
    }
}