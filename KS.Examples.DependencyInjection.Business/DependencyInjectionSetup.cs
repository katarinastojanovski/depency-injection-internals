﻿using KS.Examples.DependencyInjection.Registration;
using System.Collections.Generic;

namespace KS.Examples.DependencyInjection.Business
{
    public class DependencyInjectionSetup : IServiceItemsSetup
    {
        public IEnumerable<ServiceItem> GetAllRegisteredServices()
        {
            var titleChecker = new ServiceItem()
            {
                ServiceType = typeof(ITitleChecker),
                ImplementationType = typeof(TitleChecker),
                Lifetime = ServiceItemLifetime.Singleton
            };

            yield return titleChecker;

            var articleService = new ServiceItem()
            {
                ServiceType = typeof(IArticlesService),
                ImplementationType = typeof(ArticlesService),
                Lifetime = ServiceItemLifetime.Singleton
            };

            yield return articleService;
        }
    }
}