﻿using KS.Examples.DependencyInjection.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KS.Examples.DependencyInjection.Business
{
    public interface IArticlesService
    {
        Task<IEnumerable<Article>> GetAll();

        Task<Article> GetById(Guid id);

        Task<Article> Add(Article article);
    }
}