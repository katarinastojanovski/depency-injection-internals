﻿using System;
using System.Linq;

namespace KS.Examples.DependencyInjection.Data.InMemory
{
    internal class Randomizer : IRandomizer
    {
        private static Random random = new Random();

        public string GetString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}