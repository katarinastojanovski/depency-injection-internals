﻿namespace KS.Examples.DependencyInjection.Data.InMemory
{
    internal interface IRandomizer
    {
        string GetString(int length);
    }
}