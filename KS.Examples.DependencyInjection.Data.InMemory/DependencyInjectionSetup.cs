﻿using System.Collections.Generic;
using KS.Examples.DependencyInjection.Registration;

namespace KS.Examples.DependencyInjection.Data.InMemory
{
    public class DependencyInjectionSetup : IServiceItemsSetup
    {
        public IEnumerable<ServiceItem> GetAllRegisteredServices()
        {
            var randomizer = new ServiceItem()
            {
                ServiceType = typeof(IRandomizer),
                ImplementationType = typeof(Randomizer),
                Lifetime = ServiceItemLifetime.Singleton
            };

            yield return randomizer;

            var articleRepository = new ServiceItem()
            {
                ServiceType = typeof(IArticlesRepository),
                ImplementationType = typeof(InMemoryArticlesRepository),
                Lifetime = ServiceItemLifetime.Singleton
            };

            yield return articleRepository;
        }
    }
}