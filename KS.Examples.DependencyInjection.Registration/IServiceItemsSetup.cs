﻿using System.Collections.Generic;

namespace KS.Examples.DependencyInjection.Registration
{
    public interface IServiceItemsSetup
    {
        IEnumerable<ServiceItem> GetAllRegisteredServices();
    }
}