﻿namespace KS.Examples.DependencyInjection.Registration
{
    public enum ServiceItemLifetime
    {
        Transient,

        Scoped,

        Singleton
    }
}