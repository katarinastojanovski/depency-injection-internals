﻿using System;

namespace KS.Examples.DependencyInjection.Registration
{
    public class ServiceItem
    {
        public Type ServiceType { get; set; }

        public Type ImplementationType { get; set; }

        public ServiceItemLifetime Lifetime { get; set; }
    }
}