﻿using KS.Examples.DependencyInjection.Registration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace KS.Examples.DependencyInjection.DI
{
    public static class DependencyInjectionSetup
    {
        public static void AddMyServices(this IServiceCollection services)
        {
            var serviceItemSetups = new List<IServiceItemsSetup>
            {
                new Data.InMemory.DependencyInjectionSetup(),
                new Business.DependencyInjectionSetup()
            };

            foreach (IServiceItemsSetup serviceItemsSetup in serviceItemSetups)
            {
                foreach (ServiceItem serviceItem in serviceItemsSetup.GetAllRegisteredServices())
                {
                    // For now we will implement only singleton.
                    if (serviceItem.Lifetime == ServiceItemLifetime.Singleton)
                    {
                        services.AddSingleton(serviceItem.ServiceType, serviceItem.ImplementationType);
                    }
                }
            }
        }
    }
}